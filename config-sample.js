var config = {
  slack: {
    hook: 'https://hooks.slack.com/change/this/url',
    channel: '#ses-notifications',
    username: 'Email Notifications'
  }
}

module.exports = config
