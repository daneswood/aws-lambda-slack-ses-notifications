// Local config file
var config = require('./config')

//
var async = require('async')
var slack = require('slack-notify')(config.slack.hook)

// Process incoming notifications
exports.handler = function (event, context) {
  console.log('Running index.handler')

  // Slack notification types
  var bounceNotification = slack.extend({
    channel: config.slack.channel,
    username: config.slack.username,
    icon_emoji: ':warning:'
  })

  var complaintNotification = slack.extend({
    channel: config.slack.channel,
    username: config.slack.username,
    icon_emoji: ':exclamation:'
  })

  // Fail lambda context on slack-notify error
  slack.onError = function (err) {
    context.fail(err)
  }

  // Cycle through all of the records in this notification
  var records = (event.Records || [])

  console.log('Records to be processed: ' + records.length)

  // Wrap everything up to prevent the function completing before notifications are sent
  async.forEach((records || []), function (record, callback) {
    if (record.Sns) {
      // De-serialize message object
      var message = JSON.parse(record.Sns.Message)

      // Switch through the types of notification we're interested in
      switch (message.notificationType) {
        case 'Bounce':
          bounceNotification('```' + JSON.stringify(message, null, 2) + '```', callback)
          break

        case 'Complaint':
          complaintNotification('```' + JSON.stringify(message, null, 2) + '```', callback)
          break
      }
    } else {
      callback()
    }
  }, function (err) {
    if (err) {
      context.fail(err)
    } else {
      context.done()
    }
  })

  // We're done here.
  console.log('Stopping index.handler')
}
